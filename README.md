At [eyewearshop.com](https://eyewearshop.com/) we love every passion and interest on Earth because it is a reference to your UNIQUENESS. And to spread exactly that...is our core vision:

To help you Express Yourself. To support you at BEING YOURSELF.

Since we know you want all sort of high quality products, we got you covered with highly professional suppliers and production houses that we keep in close contact with and vet daily so that they fulfill Beyond Vault's intense selection process.
No matter where you are, who you are and what you are passionate about we want to be able to provide you with high quality products!

That's why in eyewearshop.com you will find a custom collection for every profession, hobby, sport, passion or anything you might think of.

So whatever you're looking for, we plan to have it there for you. And if it's not, then hit us up and let us know, so we can negotiate or produce the best deal for you in no time. We are and would like to be here for YOU for a lifetime.
Whatever you need, it's right here on eyewearshop.com.